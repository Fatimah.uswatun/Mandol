<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TC1_TC4</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-08-09T14:20:47</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9c5a6659-8a88-4605-8ee1-02981404a125</testSuiteGuid>
   <testCaseLink>
      <guid>7f2018ab-92c3-49bc-85fb-325f57070101</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>518b2e6e-33a8-466b-afc2-0a3efdbe772a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9dd68c24-8ebd-40c8-b56a-ffebe62fa77f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>dc6eb254-1d60-43c4-8753-054e64ea5621</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7486bfb2-8fe7-4a55-9a07-d1ac930d1a94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC3.reply</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b5de519-6818-442c-a58f-ccfeb06c1776</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC4/forward</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a147974c-f4bd-409c-b671-16b6a075a78d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
